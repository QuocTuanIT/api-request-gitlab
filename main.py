import requests
# 11468982
response = requests.get("https://gitlab.com/api/v4/users/11468982/projects")

my_project  = response.json()

for project in my_project:
    print(f"My project is : {project['name']}. Project url is : {project['web_url']}")